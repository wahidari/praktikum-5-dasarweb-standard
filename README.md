# Praktikum 5 Dasar Pemrograman Web

[![Build status](https://gitlab.com/gitlab-org/gitlab-ee/badges/master/build.svg)](https://gitlab.com/wahidari/praktikum-5-dasarweb-standard/commits/master)
[![](https://img.shields.io/badge/Find%20Me-%40wahidari-009688.svg?style=social)](https://wahidari.gitlab.io)

## Language

- [![](https://img.shields.io/badge/html-5-FF5722.svg)](https://wahidari.gitlab.io) 
- [![](https://img.shields.io/badge/css-3-03A9F4.svg)](https://wahidari.gitlab.io) 
- [![](https://img.shields.io/badge/javascript-1.8-FFCA28.svg)](https://wahidari.gitlab.io) 

## Test coverage

- [![HTML coverage](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=coverage)](https://gitlab.com/wahidari/praktikum-5-dasarweb-standard/graphs/master/charts) html
- [![JavaScript coverage](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=karma)](https://gitlab.com/wahidari/praktikum-5-dasarweb-standard/graphs/master/charts) javaScript
- [![CSS coverage](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg)](https://gitlab.com/wahidari/praktikum-5-dasarweb-standard/graphs/master/charts) css

## Screenshot

- ScreenShot 1

    ![](./ss/a.PNG)

- ScreenShot 2

    ![](./ss/b.PNG)

- ScreenShot 3

    ![](./ss/c.PNG)

## License
> This program is Free Software: You can use, study, share and improve it at your
will. Specifically you can redistribute and/or modify it under the terms of the
[GNU General Public License](https://www.gnu.org/licenses/gpl.html) as
published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.